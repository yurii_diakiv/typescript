import FightersView from "./fightersView";
import Fighter from "./fighter";

class Fight
{
    firstFighter: Fighter;
    secondFighter: Fighter;

    constructor(fighter1: Fighter, fighter2: Fighter)
    {
        this.firstFighter = fighter1;
        this.secondFighter = fighter2;
    }

    fight()
    {
        while(this.firstFighter.health > 0 && this.secondFighter.health > 0)
        {
            // setTimeout(this.firstFighterHit, 1500);
            // setTimeout(this.secondFighterHit, 1500);
            
            this.firstFighterHit();
            if(this.secondFighter.health < 0)
            {
                console.log("first fighter won!");
                break;
            }
            
            this.secondFighterHit();
            if(this.firstFighter.health < 0)
            {
                console.log("second fighter won!");
                break;
            }
        }
    }

    firstFighterHit()
    {
        let healthToSub: number = this.firstFighter.getHitPower() - this.secondFighter.getBlockPower();
        if(healthToSub < 0)
            healthToSub = 0;
        console.log("first fighter hit: " + healthToSub);
        this.secondFighter.health -= healthToSub;
        console.log("second fighter health: " + this.secondFighter.health);
    }

    secondFighterHit()
    {
        let healthToSub: number = this.secondFighter.getHitPower() - this.firstFighter.getBlockPower()
        if(healthToSub < 0)
            healthToSub = 0;
        console.log("second fighter hit: " + healthToSub);
        this.firstFighter.health -= healthToSub;
        console.log("first fighter health: " + this.firstFighter.health);
    }
}

export default Fight;