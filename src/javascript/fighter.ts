class Fighter
{
    name: string;
    health: number;
    attack: number;
    defense: number;

    constructor(fighter)
    {
        
        const { name, health, attack, defense } = fighter;
        this.name = name;
        this.health = health;
        this.attack = attack;
        this.defense = defense;
    }

    getHitPower(): number
    {
        let criticalHitChance = Math.random() + 1;
        let power = this.attack * criticalHitChance;
        return power;
    }

    getBlockPower(): number
    {
        let dodgeChance = Math.random() + 1;
        let power = this.defense * dodgeChance;
        return power;
    }

    toStr() : string
    {
        return `Name: ${this.name}, Health : ${this.health}, Attack: ${this.attack}, Defence: ${this.defense}`;
    }
}

export default Fighter;