import View from "./view";
import FighterView from "./fighterView";
import { fighterService } from "./services/fightersService";
import Fighter from "./fighter";

class FightersView extends View {
  handleClick: any;
  constructor(fighters) {
    super();
    
    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);
  }

  fightersDetailsMap = new Map();

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
  }

  async handleFighterClick(event, fighter) {
    //this.fightersDetailsMap.set(fighter._id, fighter);
    console.log('clicked');

    if(!this.fightersDetailsMap.has(fighter._id))
    {
      let fighterInfo = await fighterService.getFighterDetails(fighter._id);

      // console.log("hhh");
      
      this.fightersDetailsMap.set(fighter._id, new Fighter(fighterInfo));
    }


    this.showModal(this.fightersDetailsMap.get(fighter._id));

    // get from map or load info and add to fightersMap
    // show modal with fighter info
    // allow to edit health and power in this modal
  }

  showModal(fighter: Fighter)
  {
    let modal = document.getElementById("myModal");
    let span = document.getElementById("close1");
    let button = document.getElementById("changeButton");

    modal.style.display = "block";

    document.getElementById("nameLabel").innerHTML = "Name: " + fighter.name;
    document.getElementById("healthLabel").innerHTML = "Health: " + fighter.health;
    document.getElementById("attackLabel").innerHTML = "Attack: " + fighter.attack;
    document.getElementById("defenseLabel").innerHTML = "Defense: " + fighter.defense;
    
    

    

    button.onclick = function()
    {
      // if((<HTMLInputElement>document.getElementById("nameInput")) != null)
      let name: string = (<HTMLInputElement>document.getElementById("nameInput")).value;
      (<HTMLInputElement>document.getElementById("nameInput")).value = "";
      let health: number = +(<HTMLInputElement>document.getElementById("healthInput")).value;
      (<HTMLInputElement>document.getElementById("healthInput")).value = "";
      let attack: number = +(<HTMLInputElement>document.getElementById("attackInput")).value;
      (<HTMLInputElement>document.getElementById("attackInput")).value = "";
      let defense: number = +(<HTMLInputElement>document.getElementById("defenseInput")).value;
      (<HTMLInputElement>document.getElementById("defenseInput")).value = "";
      
      if(name != "")
        fighter.name = name;
      if(health > 0)
        fighter.health = health;
      if(attack > 0)
        fighter.attack = attack;
      if(defense > 0)
        fighter.defense = defense;

      modal.style.display = "none";
    }

    span.onclick = function()
    {
      modal.style.display = "none";
    }

    window.onclick = function(event) {
      if (event.target == modal) {
        modal.style.display = "none";
      }
    }
  }
}

export default FightersView;