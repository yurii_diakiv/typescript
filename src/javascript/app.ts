import FightersView from "./fightersView";
import { fighterService } from "./services/fightersService";
import Fight from "./fight";
import Fighter from "./fighter";

class App {
  constructor() {
    this.startApp();
  }

  static rootElement = document.getElementById('root');
  static loadingElement = document.getElementById('loading-overlay');

  static fightButton = <HTMLButtonElement>document.getElementById("fButton");
  static firtsFighterSelect = <HTMLSelectElement>document.getElementById("selectFirst");
  static secondFighterSelect = <HTMLSelectElement>document.getElementById("selectSecond");

  async startApp() {
    try {
      App.loadingElement.style.visibility = 'visible';
      
      const fighters = await fighterService.getFighters();
      const fightersView = new FightersView(fighters);
      const fightersElement = fightersView.element;

      App.rootElement.appendChild(fightersElement);

      console.log(App.firtsFighterSelect.value);

      App.firtsFighterSelect.onchange = async function()
      {
        if(App.firtsFighterSelect.value != "" && App.secondFighterSelect.value != "")
          App.fightButton.disabled = false;
        else
          App.fightButton.disabled = true;

        if(App.firtsFighterSelect.value != "")
        {
          if(!fightersView.fightersDetailsMap.has(App.firtsFighterSelect.value))
          {
            let fighterInfo = await fighterService.getFighterDetails(App.firtsFighterSelect.value);

            fightersView.fightersDetailsMap.set(App.firtsFighterSelect.value, new Fighter(fighterInfo));
          }
        }
      }

      App.secondFighterSelect.onchange = async function()
      {
        if(App.firtsFighterSelect.value != "" && App.secondFighterSelect.value != "")
          App.fightButton.disabled = false;
        else
          App.fightButton.disabled = true;

        if(App.secondFighterSelect.value != "")
        {
          if(!fightersView.fightersDetailsMap.has(App.secondFighterSelect.value))
          {
            let fighterInfo = await fighterService.getFighterDetails(App.secondFighterSelect.value);
  
            fightersView.fightersDetailsMap.set(App.secondFighterSelect.value, new Fighter(fighterInfo));
          }
        }
      }

      

      App.fightButton.onclick = function()
      {
        startFight(fightersView.fightersDetailsMap);
      }
      
      

    } catch (error) {
      console.warn(error);
      App.rootElement.innerText = 'Failed to load data';
    } finally {
      App.loadingElement.style.visibility = 'hidden';
    }

    function startFight(fightersDetailsMap: Map<string, Fighter>)
    {
      let fighter1: Fighter = fightersDetailsMap.get(App.firtsFighterSelect.value);
      console.log("fighter1:");
      console.log(fighter1);
      let fighter2: Fighter = fightersDetailsMap.get(App.secondFighterSelect.value);
      console.log("fighter2:");
      console.log(fighter2); 
      const fight1: Fight = new Fight(fighter1, fighter2);
      fight1.fight();
    }
  }

  
}

export default App;